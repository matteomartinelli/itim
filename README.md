# Hypermedia application (Web and multimedia)

 - Matteo Martinelli 840538
 - Christian Zichichi 840565

# Framework & template

 - Since we are not web-designer, we decided to use a predefined template that makes good use of jQuery, Modernizr and Bootstrap frameworks as a basis to start developing our website:

 https://demo.proteusthemes.com/repairpress/

 - Since the aforementioned template is Wordpress-ready, we decided to extract *ONLY* and *EXCLUSIVELY* basic CSS files since our website is made in *PURE* HTML without using any *CMS*.

# Documentation

Our web application makes AJAX requests to the server written in PHP. 

We have decided to use these APIs:

 - *GET* WEBSITE/php/smartlifes.php	Retrieve all the smartlife services according to the filter in parameters
 - *GET* WEBSITE/php/devices.php	Retrieve all the devices according to the filter in parameters
 - *GET* WEBSITE/php/assistances.php	Retrieve all the assistance services according to the filter in parameters

Moreover, we allow a user to perform requests through web-forms and store the requests in the database thanks to:

 - *GET* WEBSITE/php/assistance_request.php
 - *GET* WEBSITE/php/device_request.php
 - *GET* WEBSITE/php/smartlifes_request.php

These forms are actually working forms: there is a validity check in all the Email fields to guarantee that a correct email address is inserted (if @domain.tld is present). If the check is not passed, an error message will appear and the form will not be correctly submitted.

Active links are visually distinguished from inactive links (different color - blue). We tried to make the navigation as fluid and coherent as possible by having at least one active link for each page (e.g. one device/assistance service/smart life service) and using breadcrumbs (active link themselves). 
