-- MySQL dump 10.13  Distrib 5.7.12, for Linux (x86_64)
--
-- Host: localhost    Database: my_itim
-- ------------------------------------------------------
-- Server version	5.7.12-0ubuntu1.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `smartlifes_devices`
--

DROP TABLE IF EXISTS `smartlifes_devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `smartlifes_devices` (
  `smartlife_device_id` int(11) NOT NULL AUTO_INCREMENT,
  `smartlife_id` int(11) DEFAULT NULL,
  `device_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`smartlife_device_id`),
  KEY `fk_smartlifes_devices_1_idx` (`smartlife_id`),
  KEY `fk_smartlifes_devices_2_idx` (`device_id`),
  CONSTRAINT `fk_smartlifes_devices_1` FOREIGN KEY (`smartlife_id`) REFERENCES `smartlifes` (`smartlife_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_smartlifes_devices_2` FOREIGN KEY (`device_id`) REFERENCES `devices` (`device_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `smartlifes_devices`
--

LOCK TABLES `smartlifes_devices` WRITE;
/*!40000 ALTER TABLE `smartlifes_devices` DISABLE KEYS */;
INSERT INTO `smartlifes_devices` VALUES (1,1,14),(2,1,15),(3,1,16),(4,1,17),(5,1,18),(6,1,33),(7,2,1),(8,2,3),(9,2,8),(10,2,9),(11,2,10),(12,2,34),(53,3,14),(54,3,15),(55,3,16),(56,3,17),(57,3,18),(58,4,1),(59,4,3),(60,4,8),(61,4,14),(62,4,15),(63,4,16),(64,5,14),(65,5,15),(66,5,16),(67,5,17),(68,5,18),(69,6,31),(70,6,32),(71,7,1),(72,7,3),(73,7,30),(74,8,35),(75,8,36),(76,8,37),(77,8,38),(78,8,39),(79,9,33),(80,10,14),(81,10,15),(82,10,16);
/*!40000 ALTER TABLE `smartlifes_devices` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-06-29  1:00:33
