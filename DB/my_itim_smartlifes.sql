-- MySQL dump 10.13  Distrib 5.7.12, for Linux (x86_64)
--
-- Host: localhost    Database: my_itim
-- ------------------------------------------------------
-- Server version	5.7.12-0ubuntu1.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `smartlifes`
--

DROP TABLE IF EXISTS `smartlifes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `smartlifes` (
  `smartlife_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `category` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=TV&Entertainment, 1=Health, 2=Home, 3=Person',
  `description` text,
  PRIMARY KEY (`smartlife_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `smartlifes`
--

LOCK TABLES `smartlifes` WRITE;
/*!40000 ALTER TABLE `smartlifes` DISABLE KEYS */;
INSERT INTO `smartlifes` VALUES (1,'iTIMvision',0,NULL),(2,'iTIMmusic',0,'<b>iTIMmusic subscription allows to listen high quality and ad-free music<br>\nin streaming without limitation, for a fair monthly price of 7,99 €.</b><br><br><br>\n\nAnd now, if you are a new customer in iTIM, you can enjoy your first<br>\nmonth absolutely free of charge. After the first 30 days the service remains<br>\nactive at the cost of 7,99 €/month.<br><br>\n\nThe service is avalaible through the App for Android Smartphone<br>\nand Tablets, iPhone/iPad and Windows Phone and it is reserved for<br>\niTIM customers only.<br><br><br>'),(3,'iTIMreading',0,NULL),(4,'iTIMgames',0,NULL),(5,'Serie A iTIM',0,NULL),(6,'WellUP',1,'<b>Features: fitness tracking, nutrition monitoring, alcohol and<br>\nsmoke monitoring, schedule of medical check-ups, personal agenda.</b><br><br><br>\n\nWellUP subscription is required to download a license key<br>\nto insert after having installed the app on you Android or iOS device,<br>\nand make it work with wearable devices via bluetooth for a fair<br>\nmonthly price of 0,99 €.<br><br><br>\n\nAnd now, if you are a new customer in iTIM, you can enjoy your first<br>\nmonth and download the license key absolutely free of charge.<br>\nAfter the first 30 days the service remains active at 0,99 €/month.<br><br><br>\n\n'),(7,'iTIM Tag',2,NULL),(8,'iTIM Home Connect',2,'<b>The ideal solution to manage all home automation systems in your home , from simple domestic boiler , to the home protection systems , with the most advanced monitoring systems.</b><br><br><br>\n\nThe offer is meant to be used in home automation devices in the home , the management of which involves the use of a SIM. <br><br><br>\n\nConnect with iTim Home, for you available minutes , SMS and MB to be used to connect and manage your burglar alarm , your boiler or other home<br> automation systems. <br><br><br>\n\nThe offer includes every 30 days: <br><br>\n20-minute calls<br>\n100 MB of Internet traffic<br>\n100 SMS to all<br><br><br>\n\nThe offer will be activated within a maximum of 48 hours of request. <br>\nThe validity of the offer is confirmed automatically every 30 days , at the latest within four hours from midnight the monthly renewal date. <br>\nYou can check if the offer is active and the availability of services including free calling the number 40916, Customer Service 119 or accessing My-iTIM Site Mobile.<br>\nIf no active navigation data offer applies the base rate.<br><br><br>'),(9,'Digital Life Or Real Life?',3,NULL),(10,'Digital Signature',3,'<b>Accessing services, you pay for a subscription or a purchase, <br>\ndigital transactions are now part of our daily life and are gradually spreading.<br>\nReliability is a crucial component. Storage of money, as well as personal data, <br>\nmay be more convenient and also more secure.</b><br><br>\n\nDigital Signature is required for these services: <br><br>\n\n<b>Payments</b><br><br>\n\nChoose payment cards of Intesa Sanpaolo , BNL or Mediolanum : pay is simple and convenient !<br><br>\n\n<b>Transports</b><br><br>\n\nBuy transport tickets of your city , through the SMS ticketing service. Check now if the service is already available in your city ! <br><br><br>\n\n<b>Fidelity Cards</b><br><br>\n\nSave the TIM Wallet your loyalty cards to have them always with you. <br><br><br>\n\n<b>Coupons</b><br><br>\n\nSave on a wide range of products and services selected from HERE ! Group<br><br><br>');
/*!40000 ALTER TABLE `smartlifes` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-06-29  1:00:33
