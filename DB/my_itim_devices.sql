-- MySQL dump 10.13  Distrib 5.7.12, for Linux (x86_64)
--
-- Host: localhost    Database: my_itim
-- ------------------------------------------------------
-- Server version	5.7.12-0ubuntu1.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `devices`
--

DROP TABLE IF EXISTS `devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devices` (
  `device_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `device_category` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=smartphone,1=tablet,2=smart_living,3-modem',
  `price` float NOT NULL,
  `discounted_price` float DEFAULT NULL,
  `features` text,
  `description` text,
  PRIMARY KEY (`device_id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `devices`
--

LOCK TABLES `devices` WRITE;
/*!40000 ALTER TABLE `devices` DISABLE KEYS */;
INSERT INTO `devices` VALUES (1,'Apple Iphone 6S 16 GB',0,799,699,'<ul>\n<li>iOS 9 e iCloud</li>\n<li>Display 4.7\" Retina HD 1334x740px</li>\n<li>3D Touch</li>\n</ul>','OS: iOS 9<br>\nCHIPSET: Apple A9<br>\nCPU: Dual-core 1.84 GHz Twister<br>\nGPU: PowerVR GT7600<br>\nDISPLAY: 4.7\", Retina HD 1334x740px<br>\nTECHNOLOGY: 4G cat.9/HSDPA42UMTS/EDGE/GPRS<br>\nCONNECTIVITY: Wi-Fi - Bluetooth - Micro USB - NFC<br>\nCAMERA: 12 Mpixel<br>\nSTORAGE-MEMORY: 16 GB - 2 GB<br>\nDIMENSIONS: 138.3 x 67.1 x 7.1 mm<br>\nWEIGHT: 143 gr.<br>\n'),(3,'Samsung Galaxy S7 edge',0,899,NULL,'<ul>\n<li>Android OS 6.0</li>\n<li>Display 5.5\"</li>\n<li>OctaCore Processor (QuadCore 2.3 Ghz + QuadCore 1.6 Ghz)</li>\n</ul>','OS: Android 6.0<br>\nCHIPSET: Exynos 8890 Octa<br>\nCPU: QuadCore 2.3 Ghz + QuadCore 1.6 Ghz<br>\nGPU: Mali-T880 MP12<br>\nDISPLAY: 5.5\", Super AMOLED<br>\nTECHNOLOGY: 4G cat.9/HSDPA42UMTS/EDGE/GPRS<br>\nCONNECTIVITY: Wi-Fi - Bluetooth - Micro USB - NFC<br>\nCAMERA: 12 Mpixel<br>\nSTORAGE-MEMORY: 32 GB - 4 GB<br>\nDIMENSIONS: 150,9 x 72 , 6 x 7,7 mm<br>\nWEIGHT: 157 gr.<br>\n'),(8,'Microsoft Lumia 950',0,750,700,NULL,NULL),(9,'LG K10',0,249,219,NULL,NULL),(10,'Huawei P9',0,699,NULL,NULL,NULL),(14,'Acer Iconia W4',1,299,259,'<ul>\n<li>OS Windows 8.1</li>\n<li>Display 8\" (1280x800) IPS</li>\n<li>Intel Atom Quad-Core 1.8 Ghz</li>\n</ul>','OS: Windows 8.1<br>\nCPU: Intel Atom Quad-Core 1.8GHz<br>\nDISPLAY: 8\" (1280x800) IPS<br>\nCONNECTIVITY: Bluetooth 4.0<br>\nCAMERA: 5 Mpx A/F + 2 Mpx<br>\nSTORAGE-MEMORY: 32 GB - 2 GB <br>\nDIMENSIONS: 218.9 x 134.9 x 10.75 mm<br>\nWEIGHT: 420 gr.<br>'),(15,'Apple Ipad Pro 32 GB',1,845,799,NULL,NULL),(16,'Samsung Galaxy Tab A',1,599,NULL,NULL,NULL),(17,'Huawei Mediapad T1',1,249,209,NULL,NULL),(18,'Huawei Mediapad 10',1,199,NULL,NULL,NULL),(30,'Apple Watch Sport',2,499,459,NULL,NULL),(31,'iHealth MS5',2,99,NULL,NULL,NULL),(32,'Polar Loop Activity Tracker',2,99,79,'<ul>\n<li>Training Monitoring</li>\n<li>Steps Counter</li>\n<li>Calories Tracker</li>\n</ul>','POWER SOURCE: Battery<br>\nDISPLAY: Yes<br>\nTYPE: Calories Tracker - Steps Counter - Watch<br>\nCOLOR: Black<br>\nRMS POWER: 0 W<br>\nCONNECTIVITY: Bluetooth - Micro USB<br>\nDIMENSIONS: 23 x 23 x 2 cm<br>\nWEIGHT: 200 gr.<br>'),(33,'Samsung Gear VR',2,129,NULL,NULL,NULL),(34,'Beats Solo 2',2,229,209,NULL,NULL),(35,'Powerline Adapter',3,69,49,NULL,NULL),(36,'Modem WIFI iTIM',3,49,NULL,NULL,NULL),(37,'Tecnoware ERA Plus 900',3,79,NULL,NULL,NULL),(38,'Repeater ZyXEL N300',3,39,NULL,'<ul>\n<li>Wi-Fi up to 300 Mbps at 2.4 GHz</li>\n<li>For any 802.11b/g/n network</li>\n<li>One-button installation</li>\n</ul>','TECHNOLOGY: Wi-Fi b/g/n at 2.4 GHz<br>\nTRANSMISSION SPEED: Up to 300 Mbps<br>\nCONNECTIVITY: WPS Association<br>\nPORT: Ethernet<br>\nSERVICES: Online Firmware Updates<br>'),(39,'iTIM Model ADSL WIFI',3,59,39,NULL,NULL);
/*!40000 ALTER TABLE `devices` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-06-29  1:00:33
