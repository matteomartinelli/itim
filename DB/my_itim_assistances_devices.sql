-- MySQL dump 10.13  Distrib 5.7.12, for Linux (x86_64)
--
-- Host: localhost    Database: my_itim
-- ------------------------------------------------------
-- Server version	5.7.12-0ubuntu1.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `assistances_devices`
--

DROP TABLE IF EXISTS `assistances_devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assistances_devices` (
  `assistance_device_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) DEFAULT NULL,
  `assistance_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`assistance_device_id`),
  KEY `fk_assistances_devices_1_idx` (`assistance_id`),
  KEY `fk_assistances_devices_2_idx` (`device_id`),
  CONSTRAINT `fk_assistances_devices_1` FOREIGN KEY (`assistance_id`) REFERENCES `assistances` (`assistance_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_assistances_devices_2` FOREIGN KEY (`device_id`) REFERENCES `devices` (`device_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assistances_devices`
--

LOCK TABLES `assistances_devices` WRITE;
/*!40000 ALTER TABLE `assistances_devices` DISABLE KEYS */;
INSERT INTO `assistances_devices` VALUES (1,1,1),(10,3,1),(11,8,1),(12,9,1),(13,10,1),(14,1,37),(15,3,37),(16,8,37),(17,9,37),(18,10,37),(19,1,2),(20,3,2),(21,8,2),(22,9,2),(23,10,2),(24,1,3),(25,3,3),(26,8,3),(27,9,3),(28,10,3),(59,1,9),(60,3,9),(61,14,9),(62,32,9),(63,1,11),(64,1,13),(65,3,13),(66,14,13),(67,32,13),(68,1,29),(69,3,29),(70,8,29),(71,14,29),(72,15,29),(73,16,29);
/*!40000 ALTER TABLE `assistances_devices` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-06-29  1:00:33
