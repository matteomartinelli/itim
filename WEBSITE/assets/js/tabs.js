jQuery(document).ready(function () {
    jQuery("#header_tabs > div").click(function (e) {
        jQuery("#header_tabs > div").removeClass('tab-active');
        jQuery("#content_tabs > div").removeClass('show').addClass('hidden');

        jQuery(this).addClass("tab-active");
        jQuery(jQuery(this).find("a").attr('href')).removeClass('hidden').addClass("show");

        e.preventDefault();
        return false;

    })
})