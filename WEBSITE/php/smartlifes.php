<?php

require_once __DIR__ . '/config.php';

$mysqli = $config->getDb();

$success = array();

if(isset($_GET['smartlife_id'])){
    $stmt = $mysqli->prepare("SELECT * FROM smartlifes WHERE smartlife_id=?");
    $stmt->bind_param('i', $_GET['smartlife_id']);
    $stmt->execute();

    $stmt->bind_result($col1, $col2, $col3, $col4);
    $smartlife=array();
    while ( $stmt->fetch() ) {
        $smartlife = array(
            'smartlife_id' => $col1,
            'name' => $col2,
            'category' => $col3,
            'description' => $col4,
        );
    }

    $stmt = $mysqli->prepare("SELECT D.* FROM smartlifes_devices AS SD JOIN devices AS D ON D.device_id=SD.device_id WHERE smartlife_id=?");
    $stmt->bind_param('i', $_GET['smartlife_id']);
    $stmt->execute();
    //$result = $stmt->get_result();
    //$devices = $result->fetch_all(MYSQLI_ASSOC);

    $stmt->bind_result($col1, $col2, $col3, $col4, $col5, $col6, $col7);
    $devices = array();
    while ( $stmt->fetch() ) {
        $devices[] = array(
            'device_id' => $col1,
            'name' => $col2,
            'device_category' => $col3,
            'price' => $col4,
            'discounted_price' => $col5,
            'features' => $col6,
            'description' => $col7,
        );
    }

    $success = array(
        'smartlife' => $smartlife,
        'related' => array(
            'devices' => $devices
        )
    );
}else{
    if (!isset($_GET['category'])) $_GET['category']="";

    //genero la query
    $query = "";
    if($_GET['category'] != ""){
        $query = "WHERE category=?";
    }

    $stmt = $mysqli->prepare("SELECT * FROM smartlifes ".$query." ORDER BY smartlife_id ASC");

    if($_GET['category'] != ""){
        $stmt->bind_param('i', $_GET['category']);
    }

    $return = $stmt->execute();

    //$result = $stmt->get_result();
    //$success = $result->fetch_all(MYSQLI_ASSOC);
    $stmt->bind_result($col1, $col2, $col3, $col4);
    $success=array();
    while ( $stmt->fetch() ) {
        $success[] = array(
            'smartlife_id' => $col1,
            'name' => $col2,
            'category' => $col3,
            'description' => $col4,
        );
    }

}

die(json_encode(array(
    'data' => $success
)));