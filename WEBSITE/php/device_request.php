<?php

require_once __DIR__ . '/config.php';

$mysqli = $config->getDb();

$success = array();

if(isset($_GET['name']) and isset($_GET['surname']) and isset($_GET['phone']) and isset($_GET['email']) and isset($_GET['city']) and
    isset($_GET['street']) and isset($_GET['device_id'])){

    if($_GET['name'] == "" or $_GET['surname'] == "" or $_GET['email'] == "" or !filter_var($_GET['email'], FILTER_VALIDATE_EMAIL)) throw new Exception("You must provide a non-empty name, surname and email");

    $stmt = $mysqli->prepare("SELECT * FROM devices WHERE device_id=?");
    $stmt->bind_param('i', $_GET['device_id']);
    $stmt->execute();

    $stmt->bind_result($col1, $col2, $col3, $col4, $col5, $col6, $col7);
    $devices = array();
    while ( $stmt->fetch() ) {
        $devices[] = array(
            'device_id' => $col1,
            'name' => $col2,
            'device_category' => $col3,
            'price' => $col4,
            'discounted_price' => $col5,
            'features' => $col6,
            'description' => $col7,
        );
    }
    if(count($devices) != 1) throw new Exception("We didn't find your device");

    $stmt = $mysqli->prepare("INSERT INTO devices_requests (device_id,name,surname,email,phone,city,street) VALUES(?,?,?,?,?,?,?)");
    $stmt->bind_param('issssss', $_GET['device_id'],  $_GET['name'],  $_GET['surname'],  $_GET['email'],  $_GET['phone'],  $_GET['city'],  $_GET['street']);
    $stmt->execute();

}else{
    throw new Exception("You must provide all the parameters (name, surname, phone, email, city, street and device_id");
}

die(json_encode(array(
    'data' => $success
)));