<?php

class Config{
    private $host = 'localhost';
    private $username = 'root';
    private $password = 'root';
    private $name = 'my_itim';

    private $mysqli;

    public function __construct(){
        $this->mysqli = new mysqli($this->host, $this->username, $this->password, $this->name);

        if (mysqli_connect_errno()) {
            die(json_encode(array(
                'error' => array('message' => "Connect failed: " . mysqli_connect_error())
            )));
        }
    }

    public function getDb(){
        return $this->mysqli;
    }

    /*$stmt = $mysqli->prepare("INSERT INTO CountryLanguage VALUES (?, ?, ?, ?)");
    $stmt->bind_param('sssd', $code, $language, $official, $percent);

    $code = 'DEU';
    $language = 'Bavarian';
    $official = "F";
    $percent = 11.2;

    $stmt->execute();*/
}