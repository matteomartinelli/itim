<?php

require_once __DIR__ . '/config.php';

$mysqli = $config->getDb();

$success = array();

if(isset($_GET['device_id'])){
    $stmt = $mysqli->prepare("SELECT * FROM devices WHERE device_id=?");
    $stmt->bind_param('i', $_GET['device_id']);
    $stmt->execute();
    //$result = $stmt->get_result();

    $stmt->bind_result($col1, $col2, $col3, $col4, $col5, $col6, $col7);
    $device = array();
    while ( $stmt->fetch() ) {
        $device = array(
            'device_id' => $col1,
            'name' => $col2,
            'device_category' => $col3,
            'price' => $col4,
            'discounted_price' => $col5,
            'features' => $col6,
            'description' => $col7,
        );
    }

    $stmt = $mysqli->prepare("SELECT A.* FROM assistances_devices AS AD JOIN assistances AS A ON A.assistance_id=AD.assistance_id WHERE device_id=?");
    $stmt->bind_param('i', $_GET['device_id']);
    $stmt->execute();
    //$result = $stmt->get_result();
    //$assistances = $result->fetch_all(MYSQLI_ASSOC);

    $stmt->bind_result($col1, $col2, $col3, $col4, $col5, $col6);
    $assistances=array();
    while ( $stmt->fetch() ) {
        $assistances[] = array(
            'assistance_id' => $col1,
            'name' => $col2,
            'category' => $col3,
            'subcategory' => $col4,
            'highlighted' => $col5,
            'description' => $col6,
        );
    }


    $stmt = $mysqli->prepare("SELECT SL.* FROM smartlifes_devices AS SLD JOIN smartlifes AS SL ON SL.smartlife_id=SLD.smartlife_id WHERE device_id=?");
    $stmt->bind_param('i', $_GET['device_id']);
    $stmt->execute();
    //$result = $stmt->get_result();
    //$smartlifes = $result->fetch_all(MYSQLI_ASSOC);

    $stmt->bind_result($col1, $col2, $col3, $col4);
    $smartlifes=array();
    while ( $stmt->fetch() ) {
        $smartlifes[] = array(
            'smartlife_id' => $col1,
            'name' => $col2,
            'category' => $col3,
            'description' => $col4,
        );
    }

    //$device['description'] = htmlentities($device['description']);
    $success = array(
        'device' => $device,
        'related' => array(
            'assistances' => $assistances,
            'smartlifes' => $smartlifes
        )
    );
}else{
    if (!isset($_GET['device_category'])) $_GET['device_category']="";
    if (!isset($_GET['sort_price'])) $_GET['sort_price'] = 'asc';
    if (!isset($_GET['model'])) $_GET['model'] = '';
    if (!isset($_GET['discounted_price']))  $_GET['discounted_price'] = 0; //all
    //controllo il sort
    if(!in_array(strtolower($_GET['sort_price']), array('asc','desc'))) $_GET['sort_price'] = 'asc';

    //genero la query
    $query = "";
    if($_GET['device_category'] != ""){
        $query = "AND device_category=?";
    }

    if($_GET['discounted_price'] == 1){
        $query .= " AND ";
        $query .= "discounted_price IS NOT NULL";
    }
    
    $stmt = $mysqli->prepare("SELECT * FROM devices WHERE name LIKE ? ".$query." ORDER BY price " . strtolower($_GET['sort_price']));

    $model = '%'.$_GET['model'].'%';
    if($_GET['device_category'] != ""){
        $stmt->bind_param('si', $model, $_GET['device_category']);
    }else{
        $stmt->bind_param('s', $model);
    }

    $return = $stmt->execute();

    //$result = $stmt->get_result();
    //$success = $result->fetch_all(MYSQLI_ASSOC);

    $stmt->bind_result($col1, $col2, $col3, $col4, $col5, $col6, $col7);
    $success = array();
    while ( $stmt->fetch() ) {
        $success[] = array(
            'device_id' => $col1,
            'name' => $col2,
            'device_category' => $col3,
            'price' => $col4,
            'discounted_price' => $col5,
            'features' => $col6,
            'description' => $col7,
        );
    }
    
    foreach ($success as $k => $v){
        $success[$k]['description'] = htmlentities($success[$k]['description']);
    }
}

die(json_encode(array(
    'data' => $success
)));