<?php

require_once __DIR__ . '/config.php';

$mysqli = $config->getDb();

$success = array();

if(isset($_GET['name']) and isset($_GET['surname']) and isset($_GET['phone']) and isset($_GET['email']) and isset($_GET['city']) and
    isset($_GET['street']) and isset($_GET['smartlife_id'])){

    if($_GET['name'] == "" or $_GET['surname'] == "" or $_GET['email'] == "" or !filter_var($_GET['email'], FILTER_VALIDATE_EMAIL)) throw new Exception("You must provide a non-empty name, surname and email");

    $stmt = $mysqli->prepare("SELECT * FROM smartlifes WHERE smartlife_id=?");
    $stmt->bind_param('i', $_GET['smartlife_id']);
    $stmt->execute();

    $stmt->bind_result($col1, $col2, $col3, $col4);
    $smartlifes = array();
    while ( $stmt->fetch() ) {
        $smartlifes[] = array(
            'smartlife_id   ' => $col1,
            'name' => $col2,
            'category' => $col3,
            'description' => $col4,
        );
    }
    if(count($smartlifes) != 1) throw new Exception("We didn't find your smartlife service");

    $stmt = $mysqli->prepare("INSERT INTO smartlifes_requests (smartlife_id,name,surname,email,phone,city,street) VALUES(?,?,?,?,?,?,?)");
    $stmt->bind_param('issssss', $_GET['smartlife_id'],  $_GET['name'],  $_GET['surname'],  $_GET['email'],  $_GET['phone'],  $_GET['city'],  $_GET['street']);
    $stmt->execute();

}else{
    throw new Exception("You must provide all the parameters (name, surname, phone, email, city, street and smartlife_id");
}

die(json_encode(array(
    'data' => $success
)));