<?php

require_once __DIR__ . '/config.php';

$mysqli = $config->getDb();

$success = array();

if(isset($_GET['assistance_id'])){
    $stmt = $mysqli->prepare("SELECT * FROM assistances WHERE assistance_id=?");
    $stmt->bind_param('i', $_GET['assistance_id']);
    $stmt->execute();
    //$result = $stmt->get_result();

    $stmt->bind_result($col1, $col2, $col3, $col4, $col5, $col6);
    $assistance=array();
    while ( $stmt->fetch() ) {
        $assistance = array(
            'assistance_id' => $col1,
            'name' => $col2,
            'category' => $col3,
            'subcategory' => $col4,
            'highlighted' => $col5,
            'description' => $col6,
        );
    }

    $stmt = $mysqli->prepare("SELECT D.* FROM assistances_devices AS AD JOIN devices AS D ON D.device_id=AD.device_id WHERE assistance_id=?");
    $stmt->bind_param('i', $_GET['assistance_id']);
    $stmt->execute();
    //$result = $stmt->get_result();
    //$devices = $result->fetch_all(MYSQLI_ASSOC);
    $stmt->bind_result($col1, $col2, $col3, $col4, $col5, $col6, $col7);
    $devices = array();
    while ( $stmt->fetch() ) {
        $devices[] = array(
            'device_id' => $col1,
            'name' => $col2,
            'device_category' => $col3,
            'price' => $col4,
            'discounted_price' => $col5,
            'features' => $col6,
            'description' => $col7,
        );
    }

    $success = array(
        'assistance' => $assistance,
        'related' => array(
            'devices' => $devices
        )
    );
}else{
    if (!isset($_GET['category'])) $_GET['category'] = "";
    if (!isset($_GET['highlighted'])) $_GET['highlighted'] = 0; //all

    //genero la query
    $query = "";
    if($_GET['category'] != ""){
        $query = "WHERE category=?";
    }

    if($_GET['highlighted'] == 1){
        if($_GET['category'] != "") {
            $query .= " AND ";
        }else{
            $query = "WHERE ";
        }
        $query .= "highlighted = 1";
    }

    $stmt = $mysqli->prepare("SELECT * FROM assistances ".$query." ORDER BY category ASC, subcategory ASC, assistance_id ASC");

    if($_GET['category'] != ""){
        $stmt->bind_param('i', $_GET['category']);
    }

    $return = $stmt->execute();
    //$result = $stmt->get_result();
    //$success = $result->fetch_all(MYSQLI_ASSOC);
    $stmt->bind_result($col1, $col2, $col3, $col4, $col5, $col6);
    $success=array();
    while ( $stmt->fetch() ) {
        $success[] = array(
            'assistance_id' => $col1,
            'name' => $col2,
            'category' => $col3,
            'subcategory' => $col4,
            'highlighted' => $col5,
            'description' => $col6,
        );
    }
}

die(json_encode(array(
    'data' => $success
)));