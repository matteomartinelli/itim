<?php
header('Content-Type: text/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET,PUT,POST,DELETE');
header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding,x-requested-with");

require_once __DIR__ . '/classes/config.class.php';
require_once __DIR__ . '/functions.php';

$config = new Config();
$config->getDb()->set_charset('utf8');