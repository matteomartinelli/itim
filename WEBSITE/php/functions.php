<?php

function exception_handler($exception) {
    die(json_encode(array('error' => array('message' => $exception->getMessage()))));
}

set_exception_handler('exception_handler');