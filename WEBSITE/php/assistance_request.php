<?php

require_once __DIR__ . '/config.php';

$mysqli = $config->getDb();

$success = array();

if(isset($_GET['name']) and isset($_GET['surname']) and isset($_GET['phone']) and isset($_GET['email']) and isset($_GET['question']) and isset($_GET['assistance_id'])){

    if($_GET['name'] == "" or $_GET['surname'] == "" or $_GET['email'] == "" or  $_GET['question'] == "" or !filter_var($_GET['email'], FILTER_VALIDATE_EMAIL)) throw new Exception("You must provide a non-empty name, surname, question and email");

    $stmt = $mysqli->prepare("SELECT * FROM assistances WHERE assistance_id=?");
    $stmt->bind_param('i', $_GET['assistance_id']);
    $stmt->execute();

    $stmt->bind_result($col1, $col2, $col3, $col4, $col5, $col6);
    $assistances=array();
    while ( $stmt->fetch() ) {
        $assistances[] = array(
            'assistance_id' => $col1,
            'name' => $col2,
            'category' => $col3,
            'subcategory' => $col4,
            'highlighted' => $col5,
            'description' => $col6,
        );
    }
    if(count($assistances) != 1) throw new Exception("We didn't find your assistance");

    $stmt = $mysqli->prepare("INSERT INTO assistances_requests (assistance_id,name,surname,email,phone,question) VALUES(?,?,?,?,?,?)");
    $stmt->bind_param('isssss', $_GET['assistance_id'],  $_GET['name'],  $_GET['surname'],  $_GET['email'],  $_GET['phone'],  $_GET['question']);
    $stmt->execute();

}else{
    throw new Exception("You must provide all the parameters (name, surname, phone, email, question and assistance_id");
}

die(json_encode(array(
    'data' => $success
)));